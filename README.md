# README #

## Synopsis

CanvasDevUI is a GUI for assisting in KL and Canvas Node (preset) development.

![screengrab.png](https://bitbucket.org/repo/gLKjdL/images/2554819767-screengrab.png)


## Motivation

Developing extensions and creating Canvas (DFG) Nodes requires a 2-step process:

1.	Writing KL Extensions - Creating files and subfolders containing the KL used by the extension and then making a matching *.fpm.json* file.
2.	Converting the extensions into .canvas presets for use in Canvas - The **kl2dfg** tool is run with each extensions *.fpm.json* file and and output directory.

The CanvasDevUI is an attempt to make this process quicker and easier.
It offers the following functions:

*	Browse to and set the **User Extensions** path. 
*	List all extensions found in the **User Extensions** path.
*	Run test (by importing Extensions into KL) on selected or all Extensions.
*	Browse to and set the **DFG Path** - the path to Canvas User Nodes (presets).
*	Create Nodes from selected or all Extensions
*	These Nodes can be created either:
	1.  in a custom sub-path inside each Extension.  
	1.  all in the one place, specified by the **DFG Path**.
*	Optionally erase the existing DFG Path, useful for removing old or renamed Nodes.
*	Launch Canvas with a startup file.
*	Run KL on a file.

All settings are stored between runs. Once set up, to test and run Canvas extensions and nodes:

1.	Click **Test**
1.	Click **Create Nodes**
1.	Click **Run Canvas** 

## Installation

Download all the files in the repository and place them in the Fabric install folder (the extracted Fabric zip).
If this is not possible (e.g. no access to install directory), modify the canvasDev.sh and point FABRIC_DIR to the install directory.
If you have a non-standard python install, you may need to also point PYTHONPATH to your Python install directory.

To run in MSYS or Linux Bash shell navigate to the Fabric install dir and run:

```
#!bash


source canvasDev.sh
```


To run in Windows, double-click the canvasDev.bat file. This relies on having the environment.bat setup correctly.

I haven't tested thoroughly, so please let me know where it breaks.

## Contributors

Jamie Macdougall

## License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.