from __future__ import print_function
import sys, os, fnmatch, shutil, argparse, subprocess
from functools import partial

try:
    from PySide import QtCore, QtGui, QtUiTools
except:
    print('''Please ensure setupDev.sh is configured for your system. If the following environment variables point to the wrong location
    please set them manually in canvasDev.sh
    For example:
    FABRIC_DIR=/c/FabricEngine2.2.0
    export FABRIC_DIR
    PYTHONPATH=$FABRIC_DIR/Python/2.7:/c/Python27
    export PYTHONPATH    
     ''')
    print("PYTHONPATH is currently set to:", os.environ['PYTHONPATH'])
    print("FABRIC_DIR is currently set to:", os.environ['FABRIC_DIR'])
    sys.exit()

from PySide.QtGui import *
from PySide.QtCore import *
from canvasDevUI import Ui_MainWindow
try:
    import stylesheet_rc
except:
    print("Stylesheet resource file 'stylesheet_rc' is missing")

def find_files(directory, pattern):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.normpath(os.path.join(root, basename))
                yield filename

class OutputWrapper(QtCore.QObject):
    outputWritten = QtCore.Signal(object, object)

    def __init__(self, parent, stdout=True):
        QtCore.QObject.__init__(self, parent)
        if stdout:
            self._stream = sys.stdout
            sys.stdout = self
        else:
            self._stream = sys.stderr
            sys.stderr = self
        self._stdout = stdout

    def write(self, text):
        self._stream.write(text)
        self.outputWritten.emit(text, self._stdout)
        for line in text.splitlines():
            line = line.rstrip()
            if not line:
                continue
            if line.startswith("[FABRIC:") and "error" not in line.strip().lower():
                self.parent().insertHtml('<FONT COLOR=green>%s</FONT><br>'%line)
            elif "warning" in line.strip().lower():
                self.parent().insertHtml('<FONT COLOR=yellow>%s</FONT><br>'%line) 
            elif not self._stdout or "error" in line.strip().lower():
                    self.parent().insertHtml('<FONT COLOR=red>%s</FONT><br>'%line)       
            else:
                self.parent().insertHtml('<FONT COLOR=white>%s</FONT><br>'%line)
        sb = self.parent().verticalScrollBar()
        sb.setValue(sb.maximum())
        self._stream.flush()

    def __getattr__(self, name):
        return getattr(self._stream, name)

    def __del__(self):
        try:
            if self._stdout:
                sys.stdout = self._stream
            else:
                sys.stderr = self._stream
        except AttributeError:
            pass

class MainWindow(QMainWindow, Ui_MainWindow):
    UI_VERSION = 1
    def __init__(self, debug=False):
        super(MainWindow, self).__init__()
        self.debug = debug
        self.processList = [] 
        self.setupUi(self)  
        self.setupTerminal()  
        self.restoreSettings()
        self.connectSignals()
        self.loadCustomExtensions()
        self.show()

    def setupTerminal(self):     
        if not self.debug:
            sys.stdout = OutputWrapper(  self.textBrowser_terminal, True)
            sys.stderr = OutputWrapper( self.textBrowser_terminal, False )
        self.actionClearOutput = QAction("Clear", None)
        self.actionClearOutput.triggered.connect(partial(self.textBrowser_terminal.setText, "") )
        self.textBrowser_terminal.addAction(self.actionClearOutput)        
        self.textBrowser_terminal.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.textBrowser_terminal.setReadOnly(True)
        self.textBrowser_terminal.setUndoRedoEnabled(False)

    def restoreSettings(self):
        self.homePath = os.path.expanduser("~")
        self.settings = QSettings(os.path.join(self.homePath, ".canvasDevSettings.ini"), QSettings.IniFormat)
        self.restoreGeometry(self.settings.value("geometry"))
        self.restoreState(self.settings.value("state"),self.UI_VERSION) 
        self.lineEdit_canvasFile.setText(str(self.settings.value("lineEdit_canvasFile")) )
        self.lineEdit_KLFile.setText(str(self.settings.value("lineEdit_KLFile")) )
        self.lineEdit_customExtensionsPath.setText(str(self.settings.value("lineEdit_customExtensionsPath")) )
        self.lineEdit_userDFGPath.setText(str(self.settings.value("lineEdit_userDFGPath")) )
        self.checkBox_wipeExistingNodes.setChecked(self.settings.value("checkBox_wipeExistingNodes")==True or self.settings.value("checkBox_wipeExistingNodes")=="true")
        self.radioButton_createInSubPath.setChecked(self.settings.value("radioButton_createInSubPath")==True or self.settings.value("radioButton_createInSubPath")=="true")
        self.radioButton_createInUserPath.setChecked(self.settings.value("radioButton_createInUserPath")==True or self.settings.value("radioButton_createInUserPath")=="true")
        self.checkBox_showPrompts.setChecked(self.settings.value("checkBox_showPrompts")==True or self.settings.value("checkBox_showPrompts")=="true")
        self.checkBox_unguarded.setChecked(self.settings.value("checkBox_unguarded")==True or self.settings.value("checkBox_unguarded")=="true")

    def storeSettings(self):
        self.settings.setValue("geometry", self.saveGeometry()) 
        self.settings.setValue("state", self.saveState(self.UI_VERSION)) 
        self.settings.setValue("lineEdit_canvasFile", self.lineEdit_canvasFile.text())
        self.settings.setValue("lineEdit_KLFile", self.lineEdit_KLFile.text())
        self.settings.setValue("lineEdit_customExtensionsPath", self.lineEdit_customExtensionsPath.text())
        self.settings.setValue("lineEdit_userDFGPath", self.lineEdit_userDFGPath.text())
        self.settings.setValue("checkBox_wipeExistingNodes", self.checkBox_wipeExistingNodes.isChecked())
        self.settings.setValue("radioButton_createInSubPath", self.radioButton_createInSubPath.isChecked())
        self.settings.setValue("radioButton_createInUserPath", self.radioButton_createInUserPath.isChecked())
        self.settings.setValue("checkBox_showPrompts", self.checkBox_showPrompts.isChecked())
        self.settings.setValue("checkBox_unguarded", self.checkBox_unguarded.isChecked())

    def connectSignals(self):
        self.pushButton_browseCanvasFile.pressed.connect(partial(self.browseFile, "canvas"))
        self.pushButton_browseKLFile.pressed.connect(partial(self.browseFile, "klFile"))
        self.pushButton_browseCustomExtPath.pressed.connect(partial(self.browseFile, "customExtPath"))
        self.pushButton_browseUserDFGPath.pressed.connect(partial(self.browseFile, "userDFGPath"))
       
        self.lineEdit_canvasFile.textChanged.connect(self.storeSettings)
        self.lineEdit_KLFile.textChanged.connect(self.storeSettings)
        self.lineEdit_customExtensionsPath.textChanged.connect(self.loadCustomExtensions)
        self.lineEdit_userDFGPath.textChanged.connect(self.storeSettings)
        
        self.radioButton_createInSubPath.toggled.connect(self.storeSettings)
        self.radioButton_createInUserPath.toggled.connect(self.storeSettings)
        self.checkBox_wipeExistingNodes.stateChanged.connect(self.storeSettings)
        self.checkBox_showPrompts.stateChanged.connect(self.storeSettings)
        
        self.pushButton_createNodesAll.pressed.connect(partial(self.buildNodes,True))
        self.pushButton_createNodesSelected.pressed.connect(self.buildNodes)
        self.pushButton_runCanvas.pressed.connect(self.runCanvas)
        self.pushButton_runKL.pressed.connect(self.runKL)
        self.pushButton_testSelected.pressed.connect(self.testExtensions)
        self.pushButton_testAll.pressed.connect(partial(self.testExtensions,True))
        
    def browseFile(self, fileType):
        if fileType == "canvas":
            result = QFileDialog.getOpenFileName(self, 'Open Canvas file', self.lineEdit_canvasFile.text(), "Canvas Files (*.canvas)")[0]
            if result:
                self.lineEdit_canvasFile.setText(result)
        if fileType == "klFile":
            result = QFileDialog.getOpenFileName(self, 'Open KL file', self.lineEdit_KLFile.text(), "Kernel Files (*.kl)")[0]
            if result:
                self.lineEdit_KLFile.setText(result)
        if fileType == "customExtPath":
            result = QFileDialog.getExistingDirectory(self, 'Select Custom Extension Path', self.lineEdit_customExtensionsPath.text(), QFileDialog.ShowDirsOnly)
            if result:
                self.lineEdit_customExtensionsPath.setText(result)                
        if fileType == "userDFGPath":
            result = QFileDialog.getExistingDirectory(self, 'Select User DFG Path', self.lineEdit_userDFGPath.text(), QFileDialog.ShowDirsOnly)
            if result:
                self.lineEdit_userDFGPath.setText(result)
        
    def loadCustomExtensions(self):
        os.environ["FABRIC_EXTS_PATH"] = os.path.join(os.environ['FABRIC_DIR'],"Exts") + os.pathsep + self.lineEdit_customExtensionsPath.text()        
        self.listWidget_extensions.clear()
        print( "Loading Custom Extensions from %s"%self.lineEdit_customExtensionsPath.text())
        for jsonFile in find_files(self.lineEdit_customExtensionsPath.text(), '*.fpm.json'):
            item = QListWidgetItem(jsonFile)
            self.listWidget_extensions.addItem(item)
        self.storeSettings

    def testExtensions(self, all=False):
        if not all and not self.listWidget_extensions.selectedItems():
            print("No Extensions selected to test", file=sys.stderr)
            return
        requireStatements = ""
        itemList = self.listWidget_extensions.selectedItems()
        if all:
            itemList = self.listWidget_extensions.findItems('*', Qt.MatchWildcard)
        for eachItem in itemList:
            requireStatements += "require " + os.path.basename(eachItem.text()).replace(".fpm.json", "") + ";\n"
        requireStatements += "operator entry(){report('Test complete.');}"
        tempKLFile = os.path.join(self.homePath, ".testKLTemp.kl")
        try:
            with open(tempKLFile,"w+") as f:
                f.write(requireStatements)            
        except:
            print("Could not create temp file %s"%tempKLFile, file=sys.stderr)
            return
        self.runKL(tempKLFile)

    def buildNodes(self, all=False):
        if not all and not self.listWidget_extensions.selectedItems():
            print("No Extensions selected to test", file=sys.stderr)
            return
        requireStatements = ""
        itemList = self.listWidget_extensions.selectedItems()
        if all:
            itemList = self.listWidget_extensions.findItems('*', Qt.MatchWildcard)
        
        if self.checkBox_wipeExistingNodes.isChecked():
            erasePaths = [] 
            for eachItem in itemList:
                jsonFile = eachItem.text()
                if self.radioButton_createInSubPath.isChecked():
                    dfgFolder = self.lineEdit_subpath.text().strip()
                    if not dfgFolder:
                        print("No DFG subpath (folder) chosen.", file=sys.stderr)
                        return
                    dfgDir = os.path.normpath(os.path.join(os.path.dirname(jsonFile), dfgFolder))  
                else:
                    dfgDir = self.lineEdit_userDFGPath.text()
                if dfgDir not in erasePaths:
                    erasePaths.append(dfgDir) 
            for eachPath in erasePaths:
                if os.path.isdir(eachPath):
                    if self.checkBox_showPrompts.isChecked():
                        reply = QMessageBox.question(self, "Create Canvas Node", "Delete directory %s?"%eachPath, QMessageBox.Yes | QMessageBox.Cancel)
                        if reply != QMessageBox.Yes:
                            continue
                    shutil.rmtree(eachPath, ignore_errors=True)

        for eachItem in itemList:
            jsonFile = eachItem.text()
            print('Found Fabric file:', jsonFile)
            logFile = os.path.splitext(jsonFile)[0]+'.txt'
            if self.radioButton_createInSubPath.isChecked():
                dfgDir = os.path.normpath(os.path.join(os.path.dirname(jsonFile), self.lineEdit_subpath.text()))
                if not os.path.isdir(dfgDir):
                    if self.checkBox_showPrompts.isChecked():
                        reply = QMessageBox.question(self, "Create Canvas Node", "Create Folder %s For Nodes?"%dfgDir, QMessageBox.Yes | QMessageBox.Cancel)
                        if reply != QMessageBox.Yes:
                            continue
                    os.makedirs(dfgDir)  
            else:
                dfgDir = self.lineEdit_userDFGPath.text()            
                if not os.path.isdir(dfgDir):
                    os.makedirs(dfgDir)
            self.startProcess('kl2dfg',[jsonFile, dfgDir])
        
    def runKL(self, klFile=None):
        if not klFile:
            klFile = self.lineEdit_KLFile.text()
        if not os.path.isfile(klFile):
            print("KL File not found!", file=sys.stderr)
            return
        self.startProcess('kl',[klFile, ])

    def runCanvas(self):
        canvasApp = os.path.join(os.environ['FABRIC_DIR'],"bin", "canvas.py")
        canvasFile = self.lineEdit_canvasFile.text().strip()
        if canvasFile and not os.path.isfile(canvasFile):
            print("Canvas File not found!", file=sys.stderr)
            return
        userDFGPath = self.lineEdit_userDFGPath.text()
        if os.path.isdir(userDFGPath):
            os.environ["FABRIC_DFG_PATH"] = os.path.join(os.environ['FABRIC_DIR'],"Presets", "DFG") + os.pathsep + userDFGPath
        args = []
        args.extend(["-u", canvasApp])
        if self.checkBox_unguarded.isChecked():
            args.append("-u")
        if canvasFile:
            args.append(canvasFile) 
        self.startProcess('python', args, launchMsg="Started Canvas with %s"%" ".join(args))

    def startProcess(self, processName, argsList, launchMsg=""):
        self.processList.append(QProcess(self))
        self.processList[-1].readyReadStandardOutput.connect(partial(self.stdoutReady, processName))
        self.processList[-1].readyReadStandardError.connect(partial(self.stderrReady, processName))
        self.processList[-1].finished.connect(partial(self.removeFinishedProc, self.processList[-1]))
        if launchMsg:
            self.processList[-1].started.connect(lambda: print(launchMsg))
        self.processList[-1].start(processName, argsList) 
        if not self.processList[-1].waitForStarted():
            print("Failed to launch process: %s %s"%(processName, " ".join(argsList)), file=sys.stderr)

    def removeFinishedProc(self, proc, result):
        if proc in self.processList:
            self.processList.remove(proc)

    def stdoutReady(self, procID="Canvas"):
        for eachProcess in self.processList:
            out = str(eachProcess.readAllStandardOutput()).strip()
            if out:
                print(out + "\n")

    def stderrReady(self, procID="Canvas"):
        for eachProcess in self.processList:
            out = str(eachProcess.readAllStandardError()).strip()
            if out:
                print(out + "\n", file=sys.stderr)

    def closeEvent(self, event):
        self.storeSettings()
        super(MainWindow, self).closeEvent(event)

def process_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', action="store_true")  # optional flag
    parsed_args, unparsed_args = parser.parse_known_args()
    return parsed_args, unparsed_args

if __name__ == '__main__':
    parsed_args, unparsed_args = process_args()    
    qt_args = sys.argv[:1] + unparsed_args 
    app = QApplication(qt_args)
    app.setWindowIcon(QIcon(":/qss_icons/rc/undock.png"))
    mainWin = MainWindow(debug=parsed_args.debug)
    if os.path.isfile('stylesheet.qss'):
        with open('stylesheet.qss') as f:
            stylesheet = f.read()
        app.setStyleSheet(stylesheet)
    else:
        print("Stylesheet QSS file stylesheet.qss is missing")
    sys.exit( app.exec_() )
