#
# Script to set necessary environment variables then launch the FabricDev UI.
# Manually point PYTHONPATH, FABRIC_DIR to point to the correct installation dir if you
# encounter issues
#
unix_to_windows_path()
{
  echo "$@" | sed 's/^\/\(.\)\//\1:\\/' | sed 's/\//\\/g'
}

windows_to_unix_path() {
    echo "$1" \
    | sed -e 's/\\/\//g' -e 's/://'
}


PYTHONPATH=$(python -c 'import sys,os; print os.path.dirname(sys.executable)')


if [ -z "$PYTHONPATH" ]; then
    echo "Please add PYTHONPATH to your environment variables and point it to your Python installation."
    return
fi 

echo "Loading Canvas dev environment:"
FABRIC_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export FABRIC_DIR
echo "  Export FABRIC_DIR=\"$FABRIC_DIR\""

PATH=$FABRIC_DIR/bin:$PATH
export PATH
echo "  Export PATH=\"$PATH\""

PYTHON_VERSION=$(python -c 'import sys; print "%u.%u" % sys.version_info[:2]')
FABRIC_PYPATH=$FABRIC_DIR/Python/$PYTHON_VERSION
FABRIC_OS=$(uname -s)
if [[ "$FABRIC_OS" == *W32* ]] || [[ "$FABRIC_OS" == *W64* ]] && [[ "$FABRIC_OS" != *MINGW32_NT* ]]; then
  PYTHONPATH="$(unix_to_windows_path $FABRIC_PYPATH);$PYTHONPATH"
elif [[  "$FABRIC_OS" == *MINGW32_NT* ]]; then
  PYTHONPATH=$FABRIC_PYPATH:/$(windows_to_unix_path $PYTHONPATH)
else
  PYTHONPATH=$FABRIC_PYPATH:$PYTHONPATH
fi

export PYTHONPATH
echo "  Export PYTHONPATH=\"$PYTHONPATH\""

# FABRIC_EXTS_PATH=$FABRIC_DIR/Exts
# export FABRIC_EXTS_PATH
# echo "  Export FABRIC_EXTS_PATH=\"$FABRIC_EXTS_PATH\""

FABRIC_DFG_PATH=$FABRIC_DIR/Presets/DFG
export FABRIC_DFG_PATH
echo "  Export FABRIC_DFG_PATH=\"$FABRIC_DFG_PATH\""

python -u canvasDev.py $*
