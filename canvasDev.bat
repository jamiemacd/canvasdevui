@ECHO OFF

REM
REM Launches the Canvas Development UI. Requires Python and PySide.
REM

CALL environment.bat
CALL python canvasDev.py
